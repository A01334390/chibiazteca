//
//  ContentView.swift
//  ChibiAzteca
//
//  Created by Fernando Martin Garcia Del Angel on 30/06/20.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Azteca Utilities").padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
